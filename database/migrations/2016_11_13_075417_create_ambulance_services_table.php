<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmbulanceServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ambulance_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ambulance_request_id');
            $table->string('dispatch_time');
            $table->string('return_time')->nullable();
            $table->integer('total_time')->nullable();
            $table->string('status');//enum('status',['active','complete']);
            $table->double('cost');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ambulance_services');
    }
}
