<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
			$table->string('title');
            $table->string('firstname');
			$table->string('middlename')->nullable();
			$table->string('lastname')->nullable();
			$table->string('town')->nullable();
			$table->string('address')->nullable();
			//$table->string('tribe')->nullable();
			//$table->string('occupation')->nullable();
			$table->string('homePhone')->nullable();
			$table->string('mobile')->nullable();
			$table->string('gender');
			$table->string('maritalStatus')->nullable();
            $table->string('email')->unique();
            $table->string('password');
			$table->string('picture')->nullable();
			$table->string('group');
			
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
