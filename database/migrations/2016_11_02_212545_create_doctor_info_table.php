<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('doctor_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('department');
			$table->string('specialistArea');
			$table->text('medicalQualification');
			$table->string('cv');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::drop('doctor_info');
    }
}
