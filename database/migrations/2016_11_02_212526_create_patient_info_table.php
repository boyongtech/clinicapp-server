<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('patient_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('tribe')->nullable();
			$table->string('height');
			$table->string('weight');
			$table->text('notes');
			$table->string('isSmoker');
			$table->string('cigarettesPerDay')->nullable();
			$table->string('alcoholUnitsPerWeek')->nullable();
			$table->string('exerciseTimesPerWeek');
			$table->text('exerciseType')->nullable();
			$table->string('patientType');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::drop('patient_info');
    }
}
