<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('doctor');
			$table->string('title');
			$table->string('start');
			$table->string('end')->nullable();
			$table->integer('patient');
			$table->text('desc')->nullable();
			$table->string('type');
			$table->string('status')->nullable();
			$table->string('startDate')->nullable();
			$table->string('endDate')->nullable();
			$table->string('createdBy');
            $table->timestamps();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
