<?php

namespace App\Models\Doctor;

use Illuminate\Database\Eloquent\Model;

class DoctorInfo extends Model
{
    //
	protected $table = 'doctor_info';
	
	protected $fillable = [
	    'user_id', 'department', 'specialistArea',
		'medicalQualification', 'cv'
	];
	
	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
