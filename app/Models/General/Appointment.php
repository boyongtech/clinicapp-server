<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    //
	protected $fillable = [
	    'doctor', 'title', 'start', 'end', 'patient', 'type',
		'desc', 'status', 'startDate', 'endDate', 'createdBy'
	];
}
