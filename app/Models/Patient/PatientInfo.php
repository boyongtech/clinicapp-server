<?php

namespace App\Models\Patient;

use Illuminate\Database\Eloquent\Model;

class PatientInfo extends Model
{
    //
	protected $table = 'patient_info';
	
	protected $fillable = [
	    'user_id', 'tribe', 'height', 'weight', 'notes', 'isSmoker',
		'cigarettesPerDay', 'alcoholUnitsPerWeek', 'exerciseTimesPerWeek',
		'exerciseType', 'patientType'
	];
	
	public function user()
	{
		$this->belongsTo('App\User', 'foreign_key');
	}
}
