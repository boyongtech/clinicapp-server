<?php

namespace App\Models\Ambulance;

use Illuminate\Database\Eloquent\Model;

class AmbulancePrice extends Model
{
    protected $fillable = [
        'ambulance_id',
        'unit_time',
        'cost'
    ];
    public function ambulance(){
        return $this->belongsTo('App\Models\Ambulance\Ambulance');
    }
}
