<?php

namespace App\Models\Ambulance;

use Illuminate\Database\Eloquent\Model;

class AmbulanceService extends Model
{
    protected $fillable = [
        'ambulance_request_id',
        'dispatch_time',
        'return_time',
        'total_time',
        'status',
        'cost'
    ];

    public function ambulanceRequest(){
        return $this->belongsTo('App\Models\Ambulance\AmbulanceRequest');
    }
}
