<?php

namespace App\Models\Ambulance;

use Illuminate\Database\Eloquent\Model;

class AmbulanceRequest extends Model
{
    protected $fillable = [
        'ambulance_id',
        'dispatch_date',
        'status',
        'patient_id'
    ];

    public function ambulanceService(){
        return $this->hasOne('App\Models\Ambulance\AmbulanceService');
    }
    public function ambulance(){
        return $this->belongsTo('App\Models\Ambulance\Ambulance');
    }
}
