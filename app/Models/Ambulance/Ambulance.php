<?php

namespace App\Models\Ambulance;

use Illuminate\Database\Eloquent\Model;

class Ambulance extends Model
{
    public $timestamps = false;
    protected $fillable = ['registration_num','driver_id'];

    public function requests(){
        return $this->hasMany('App\Models\Ambulance\AmbulanceRequest');
    }
    public function ambulancePrice(){
        return $this->hasOne('App\Models\Ambulance\AmbulancePrice');
    }
}
