<?php

namespace App\Models\Prescription;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    //
    protected $fillable = [
    		'patient', 'treatment_id', 'case_history', 
    		'medication', 'dosage', 'notes', 'status', 'doctor'
    ];
}
