<?php

namespace App\Models\Prescription;

use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
    //
    protected $fillable = [
    		'name', 'description', 'cost'
    ];
}
