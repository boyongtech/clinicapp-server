<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    
	    //passport routes
		Passport::routes();
		 
		//set token expiry time
		//Passport::tokensExpireIn(Carbon::now()->addDays(15));

		//Set expiry time of refresh token
        //Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
        //
		
		//prunes revoked token
		//Passport::pruneRevokedTokens();
    }
}
