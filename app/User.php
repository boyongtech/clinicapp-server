<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'firstname', 'middlename', 'lastname', 'email', 'password',
		'town', 'address', 'homePhone', 'mobile', 'maritalStatus',
		'gender', 'picture', 'group', 'dob'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	public function doctorInfo()
	{
		return $this->hasOne('App\Models\DoctorInfo', 'foreign_key');
	}
}
