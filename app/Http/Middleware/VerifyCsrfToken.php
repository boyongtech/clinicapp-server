<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
		'/*', // this is to be remove once the api of the app is organized properly
		'api/*',
		'register',
		'login',
		'password/email',
		'password/reset',
		'logout'
		
    ];
}
