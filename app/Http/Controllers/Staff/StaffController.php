<?php

namespace App\Http\Controllers\Staff;

use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		//
		
		return response()->json([
		    'doctor' => User::where('group', '=', 'Doctor')->get(),
			'nurse' => User::where('group', '=', 'Nurse')->get(),
			'receptionist' => User::where('group', '=', 'Receptionist')->get(),
			'Accountant' => User::where('group', '=', 'Accountant')->get(),
			'Driver'   => User::where('group', '=', 'Driver')->get()
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$data = $request->all();
		
		$staff = User::create([
		    'title' => $data['title'],
		    'firstname' => $data['firstname'], 
			'middlename' => $data['middlename'], 
			'lastname' => $data['lastname'], 
			'email' => $data['email'], 
			'password' => bcrypt($data['password']),
		    'town' => $data['town'], 
			'address' => $data['address'], 
			'homePhone' => $data['homePhone'], 
			'mobile' => $data['mobile'], 
			'maritalStatus' => $data['maritalStatus'],
		    'gender' => $data['gender'], 
			//'picture' => $data['picture'], 
			'group' => $data['group'],
			'dob' => $data['dob']
		]);
		
		
		return response()->json([
		    'staff' => $staff
		]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		//
		return response()->json([
		    'user' => User::find($id)
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$data = $request->all();
		$staff = User::where('id', $id)->update([
		    'title' => $data['title'],
		    'firstname' => $data['firstname'], 
			'middlename' => $data['middlename'], 
			'lastname' => $data['lastname'], 
			'email' => $data['email'], 
			//'password' => bcrypt($data['password']),
		    'town' => $data['town'], 
			'address' => $data['address'], 
			'homePhone' => $data['homePhone'], 
			'mobile' => $data['mobile'], 
			'maritalStatus' => $data['maritalStatus'],
		    'gender' => $data['gender'], 
			'picture' => $data['picture'], 
			'group' => $data['group'],
			'dob' => $data['dob']
		]);
		
		return response()->json([
		    'staff'=> User::find($id)
		]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
