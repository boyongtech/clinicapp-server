<?php

namespace App\Http\Controllers\Ambulance;

use App\Http\Controllers\Controller;

use App\Models\Ambulance\AmbulanceRequest;
use Illuminate\Http\Request;

class AmbulanceRequestController extends Controller
{
    /**
     * @api {get} /v1/ambulance-request/ getAmbulanceRequest
     * @apiVersion 0.1.0
     * @apiName getAmbulanceRequest
     * @apiGroup AmbulanceRequest
     *
     * @apiParam {integer} [id]
     *
     */
    public function index() {
        return response()->json(AmbulanceRequest::with('ambulanceService','ambulance')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * @api {post} /v1/ambulance-request/ storeAmbulanceRequest
     * @apiVersion 0.1.0
     * @apiName storeAmbulanceRequest
     * @apiGroup AmbulanceRequest
     *
     * @apiParam {integer} ambulance_id
     * @apiParam {datetime} dispatch_time
     * @apiParam {id} patient_id
     * @apiParam {enum="active","complete"} status
     *
     */
    public function store(Request $request) {
        $ambulanceRequest = new AmbulanceRequest;
        $ambulanceRequest->fill($request->all());
        $ambulanceRequest->save();
        return response()->json($ambulanceRequest);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return response()->json(AmbulanceRequest::with('ambulanceService','ambulance')->findorfail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * @api {put} /v1/ambulance-request/:id updateAmbulanceRequest
     * @apiVersion 0.1.0
     * @apiName updateAmbulanceRequest
     * @apiGroup AmbulanceRequest
     *
     * @apiParam {integer} id ID of object ambulance request in URL
     *
     *
     * @apiParam {integer} ambulance_id
     * @apiParam {datetime} dispatch_time
     * @apiParam {integer} patient_id
     * @apiParam {enum="active","complete"} status
     *
     */
    public function update(Request $request, $id) {
        $ambulanceRequest = AmbulanceRequest::findorfail($id);
        $ambulanceRequest->fill($request->all());
        $ambulanceRequest->save();
        return response()->json($ambulanceRequest);
    }

    /**
     * @api {delete} /v1/ambulance-request/:id deleteAmbulanceRequest
     * @apiVersion 0.1.0
     * @apiName deleteAmbulanceRequest
     * @apiGroup AmbulanceRequest
     *
     *
     */
    public function destroy($id) {
        AmbulanceRequest::findorfail($id)->delete();
        return response()->json("ok");
    }
}
