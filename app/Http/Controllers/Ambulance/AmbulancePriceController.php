<?php

namespace App\Http\Controllers\Ambulance;

use App\Http\Controllers\Controller;

use App\Models\Ambulance\AmbulancePrice;
use Illuminate\Http\Request;

class AmbulancePriceController extends Controller
{
    /**
     * @api {get} /v1/ambulance-price/ getAmbulancePrice
     * @apiVersion 0.1.0
     * @apiName getAmbulancePrice
     * @apiGroup AmbulancePrice
     *
     * @apiParam {integer} [id]

     *
     *
     */
    public function index() {
        return response()->json(AmbulancePrice::with('ambulance')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * @api {post} /v1/ambulance-price/ storeAmbulancePrice
     * @apiVersion 0.1.0
     * @apiName storeAmbulancePrice
     * @apiGroup AmbulancePrice
     *
     * @apiParam {integer} ambulance_id
     * @apiParam {datetime} unit_time
     * @apiParam {double} cost
     *
     *
     */
    public function store(Request $request) {
        $ambulancePrice = new AmbulancePrice;
        $ambulancePrice->fill($request->all());
        $ambulancePrice->save();
        return response()->json($ambulancePrice);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return response()->json(AmbulancePrice::with('ambulance')->findorfail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * @api {put} /v1/ambulance-price/:id deleteAmbulancePrice
     * @apiVersion 0.1.0
     * @apiName deleteAmbulancePrice
     * @apiGroup AmbulancePrice
     *
     * @apiParam {integer} [id]
     *
     *
     */
    public function update(Request $request,AmbulancePrice $ambulancePrice) {
        $ambulancePrice->fill($request->all());
        $ambulancePrice->save();
        return response()->json($ambulancePrice);
    }

    /**
     * @api {delete} /v1/ambulance-price/:id deleteAmbulance
     * @apiVersion 0.1.0
     * @apiName deleteAmbulancePrice
     * @apiGroup AmbulancePrice
     *
     * @apiParam {integer} id
     *
     *
     */
    public function destroy($id) {
        AmbulancePrice::findorfail($id)->delete();
        return response()->json("ok");
    }
}
