<?php

namespace App\Http\Controllers\Ambulance;

use App\Http\Controllers\Controller;

use App\Models\Ambulance\Ambulance;


use App\User;
use Illuminate\Http\Request;

class AmbulanceController extends Controller
{
    /**
     * @api {get} /v1/ambulance/:id getAmbulance
     * @apiVersion 0.1.0
     * @apiName getAmbulance
     * @apiGroup Ambulance
     *
     * @apiParam {integer} [id]
     *
     */
    public function index() {
        return response()->json(Ambulance::with('requests','ambulancePrice')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * @api {post} /v1/ambulance storeAmbulance
     * @apiVersion 0.1.0
     * @apiName storeAmbulance
     * @apiGroup Ambulance
     *
     * @apiParam {integer} registration_num
     * @apiParam {integer} driver_id
     *
     */
    public function store(Request $request) {
        $ambulance = new Ambulance;
        $ambulance->fill($request->all());
        $ambulance->save();
        return response()->json($ambulance);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return response()->json(Ambulance::with('requests','ambulancePrice')->findorfail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * @api {put} /v1/ambulance/:id storeAmbulance
     * @apiVersion 0.1.0
     * @apiName storeAmbulance
     * @apiGroup Ambulance
     *
     * @apiParam {integer} id
     *
     * @apiParam {integer} registration_num
     * @apiParam {integer} driver_id
     *
     */
    public function update(Request $request,Ambulance $ambulance) {
        $ambulance->fill($request->all());
        $ambulance->save();
        return response()->json($ambulance);
    }

    /**
     * @api {delete} /v1/ambulance/:id deleteAmbulance
     * @apiVersion 0.1.0
     * @apiName deleteAmbulance
     * @apiGroup Ambulance
     *
     * @apiParam {integer} id
     *
     *
     */
    public function destroy($id) {
        Ambulance::findorfail($id)->delete();
        return response()->json('ok');
    }
    
    /**
     * 
     */
    public function serveDriver()
    {
    	$users = User::where('group', 'Driver')->get();
    		
    	foreach ($users as $user)
    	{
    		$data[] = ['label'=> $user->firstname .' '. $user->middlename .' '. $user->lastname, 'value' => $user->id];
    	}
    	
    	$data[] = ['label'=> 'Type driver\'s name', 'value' => 'one'];
    	return response()->json([
    			'drivers' => $data
    	]);
    }
}
