<?php

namespace App\Http\Controllers\Ambulance;

use App\Http\Controllers\Controller;

use App\Models\Ambulance\AmbulanceService;
use App\Models\Ambulance\Ambulance;
use App\Models\Ambulance\AmbulancePrice;
use App\Models\Ambulance\AmbulanceRequest;

use Illuminate\Http\Request;

class AmbulanceServiceController extends Controller
{
    /**
     * @api {get} /v1/ambulance-service/ getAmbulanceService
     * @apiVersion 0.1.0
     * @apiName getAmbulanceService
     * @apiGroup AmbulanceService
     *
     * @apiParam {integer} [id]
     *
     */
    public function index() {
        return response()->json(AmbulanceService::with('ambulanceRequest')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * @api {post} /v1/ambulance-service/ storeAmbulanceService
     * @apiVersion 0.1.0
     * @apiName storeAmbulanceService
     * @apiGroup AmbulanceService
     *
     * @apiParam {integer} ambulance_request_id
     * @apiParam {datetime} dispatch_time
     * @apiParam {datetime} return_time
     * @apiParam {datetime} total_time
     * @apiParam {enum="active","complete"} status
     * @apiParam {double} cost
     *
     */
    public function store(Request $request) {
        $ambulanceService = new AmbulanceService;
        $ambulanceService->fill($request->all());
        $ambulanceService->save();
        return response()->json($ambulanceService);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return response()->json(AmbulanceService::with('ambulanceRequest')->findorfail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * @api {put} /v1/ambulance-service/:id updateAmbulanceService
     * @apiVersion 0.1.0
     * @apiName updateAmbulanceService
     * @apiGroup AmbulanceService
     *
     * @apiParam {integer} id ID object of ambulance service
     *
     * @apiParam {integer} ambulance_request_id
     * @apiParam {datetime} dispatch_time
     * @apiParam {datetime} return_time
     * @apiParam {datetime} total_time
     * @apiParam {enum="active","complete"} status
     * @apiParam {double} cost
     *
     */
    public function update(Request $request, $id) {
    	//$data  = $request->all();
        $ambulanceService = AmbulanceService::updateOrCreate(['ambulance_request_id' => $id], $request->all());
        
        return response()->json($ambulanceService);
    }

    /**
     * @api {delete} /v1/ambulance-service/:id deleteAmbulanceService
     * @apiVersion 0.1.0
     * @apiName deleteAmbulanceService
     * @apiGroup AmbulanceService
     *
     * @apiParam {integer} id ID object of ambulance service
     *
     */
    public function destroy($id) {
        AmbulanceService::findorfail($id)->delete();
        return response()->json("ok");
    }
    
    /**
     * Serve price, ambulance and request info
     */
    public function servePriceInfo($ambulance_id, $ambulance_request_id)
    {
    	$price = AmbulancePrice::where('ambulance_id', $ambulance_id)->first();
    	$request = AmbulanceRequest::find($ambulance_request_id);
    	$service = AmbulanceService::where('ambulance_request_id', $ambulance_request_id)->first();
        $ambulance = Ambulance::find($ambulance_id);
        
        return response()->json(['price'=>$price, 'request'=>$request, 'ambulance'=>$ambulance, 'service'=> $service ]);
    }
}
