<?php

namespace App\Http\Controllers\Prescription;

use App\Models\Prescription\Treatment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TreatmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    	return response()->json([
    			'treatments' => Treatment::all()
    	]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data  = $request->all();
        Treatment::create([
        		'name' => $data['name'],
        		'description' => $data['description'],
        		'cost' => $data['cost']
        ]);
        
        return response()->json([
        		'status' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json([
        		'treatment' => Treatment::find($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    	$data  = $request->all();
    	Treatment::where('id', $id)->update([
    			'name' => $data['name'],
    			'description' => $data['description'],
    			'cost' => $data['cost']
    	]);
    	
    	return response()->json([
    			'status' => 'success'
    	]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $treatment = Treatment::find($id);
        $treatment->delete();
        
        return response()->json([
        		'status' => 'success'
        ]);
    }
    
    //serve data needed for auto complete form
    public function serveTreatment()
    {
    	$treatments=Treatment::all();
    
    	foreach ($treatments as $treatment)
    	{
    		$data[] = ['label'=> $treatment->name , 'value' => $treatment->id];
    	}
    
    	$data[] = ['label'=> 'Select treatment', 'value' => 'one'];
    	return response()->json([
    			'treatments' => $data
    	]);
    }
}
