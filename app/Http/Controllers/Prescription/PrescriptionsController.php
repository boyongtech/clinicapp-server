<?php

namespace App\Http\Controllers\Prescription;

use App\Models\Prescription\Prescription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrescriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json([
        		'prescriptions' => Prescription::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        
        
        $prescription = Prescription::create([
        		'patient' => $data['patient'], 
        		'treatment_id' => $data['treatment_id'],
        		'case_history' => $data['case_history'],
        		'medication' => $data['medication'],
        		'dosage' => $data['dosage'],
        		'notes' => $data['notes'],
        		'status' => 'new',
        		'doctor' => 7,
        ]);
        
        return response()->json([
        		'status'=> 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    	return response()->json([
    			'prescription' => Prescription::find($id)
    	]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    	$data = $request->all();
    	$prescription = Prescription::where('id', $id)->update([
    			'patient' => $data['patient'],
    			'treatment_id' => $data['treatment_id'],
    			'case_history' => $data['case_history'],
    			'medication' => $data['medication'],
    			'dosage' => $data['dosage'],
    			'notes' => $data['notes'],
    			'status' => 'new',
    			//'doctor' => $data['doctor'], //include later
    	]);
    	
    	return response()->json([
    			'status'=> 'success'
    	]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pres = Prescription::find($id);
        $pres->delete();
        
    	return response()->json([
    			'status'=> 'success'
    	]);
    }
    
    public function patientPrescription($id)
    {
    	return Prescription::where('patient', $id)->get();
    }
    
}
