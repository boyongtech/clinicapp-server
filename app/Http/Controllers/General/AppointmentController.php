<?php

namespace App\Http\Controllers\General;

use App\Models\General\Appointment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppointmentController extends Controller
{
    //
	public function index()
	{
		$appoints = Appointment::all();
		$calendar = \DB::table('appointments')->select('title', 'start', 'end', 'desc')->get();
		/*
		foreach ($appoints as $appt)
		{
			$appt->start = new \Date($appt->start);
			$appt->end = new \Date($appt->end);
		}
		*/
		
		return response()->json([
		    'appointments' => $appoints,
		    'calendar'  => $calendar
		]);
	}
	
	public function show($id)
	{
		return response()->json([
		    'appointment' => Appointment::find($id)
		]);
	}
	
	public function store(Request $request)
	{
		$data = $request->all();
		$appointment = Appointment::create([
		    'doctor' => $data['doctor'],
		    'title'  => $data['title'],
			'start'  => $data['start'],
			'end'  => $data['end'],
			'patient' => $data['patient'],
			'type' => $data['type'],
			'status' => 'new',
			'desc' => $data['desc'],
			'startDate' => $data['startDate'],
			'endDate' => $data['endDate'],
			'createdBy' => 1  //obtained from session
		]);
		
		return response()->json([
		    'data' => 'success'
				
		]);
	}
	
	public function update($id, Request $request)
	{
		$data = $request->all();
		$appointment = Appointment::where('id', $id)->update([
				'doctor' => $data['doctor'],
				'title'  => $data['title'],
				'start'  => $data['start'],
				'end'  => $data['end'],
				'patient' => $data['patient'],
				'type' => $data['type'],
				'status' => 'new',
				'desc' => $data['desc'],
				'createdBy' => 1  //form session
		]);
		
		return response()->json([
		    'data' => 'success'
		]);
	}
	
	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$info = Appointment::find($id);
		$info->delete();
		
		return response()->json([
		    'data' => 'success',
		]);
		
    }
	
	//serve data needed for auto complete form
	public function servePatient()
	{
		$users = \DB::table('users')
                    ->where('group', 'In patient')
                    ->orWhere('group', 'Out patient')
                    ->get();
					
		foreach ($users as $user)
		{
			$data[] = ['label'=> $user->firstname .' '. $user->middlename .' '. $user->lastname, 'value' => $user->id];
		}
		
		$data[] = ['label'=> 'Type patient\'s name' , 'value' => 'one'];
		
		return response()->json([
		    'patients' => $data
		]);
	}
	
	//serve data needed for auto complete form
	public function serveDoctorOrNurse()
	{
		$users = \DB::table('users')
                    ->where('group', 'Doctor')
                    ->orWhere('group', 'Nurse')
                    ->get();
					
		foreach ($users as $user)
		{
			$data[] = ['label'=> $user->firstname .' '. $user->middlename .' '. $user->lastname, 'value' => $user->id];
		}
		
		$data[] = ['label'=> 'Type doctor/nurse\'s name', 'value' => 'one'];
		return response()->json([
		    'doctors' => $data
		]);
	}
	
	public function doctorAppointments($id)
	{
		return response()->json([
			'appointments' => Appointment::where('doctor', $id)->get()
				
		]);
	}
	
	public function patientAppointment($id)
	{
		return Appointment::where('patient', $id)->get();
	}
}
