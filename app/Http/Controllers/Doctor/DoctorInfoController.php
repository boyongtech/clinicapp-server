<?php

namespace App\Http\Controllers\Doctor;

use App\User;

use App\Models\Doctor\DoctorInfo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoctorInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$infos = DoctorInfo::all();
		
		foreach ($infos as $info){
			$info->userInfo = $info->user();
		}
        //
		return response()->json([
		    'data' => $infos,
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
		return response()->json([
		    'csrf_token' => csrf_token()
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$data = $request->all();
		
		DoctorInfo::create([
		    'user_id' => $data['user_id'],
            'department' => $data['department'],
            'specialistArea' => $data['specialistArea'],
            'medicalQualification' => $data['medicalQualification'],
            'cv' => $data['cv'],			
		]);
		
		return response()->json([
		    'status' => 'success'
		]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		return response()->json([
		    'data' => DoctorInfo::find($id),
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		return response()->json([
		    'data' => DoctorInfo::find($id),
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$data = $request->all();
		
		DoctorInfo::where('id', $id)->update([
		    'user_id' => $data['user_id'],
            'department' => $data['department'],
            'specialistArea' => $data['specialistArea'],
            'medicalQualification' => $data['medicalQualification'],
            'cv' => $data['cv'],
		]);
		
		return response()->json([
		    'data' => 'success',
		]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$info = DoctorInfo::find($id);
		$info->delete();
		
		return response()->json([
		    'data' => 'success',
		]);
		
    }
}
