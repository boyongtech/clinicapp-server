<?php

namespace App\Http\Controllers\Patient;

use App\Models\Patient\PatientInfo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PatientInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$infos = PatientInfo::all();
		
		foreach ($infos as $info){
			$info->userInfo = $info->user();
		}
        //
		return response()->json([
		    'data' => $infos,
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		return response()->json([
		    'csrf_token' => csrf_token()
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$data = $request->all();
		
		$p = PatientInfo::create([
		    'user_id' => $data['user_id'], 
			'tribe'=> $data['tribe'], 
			'height' => $data['height'], 
			'weight'  => $data['weight'], 
			'notes'  => $data['notes'], 
			'isSmoker' => $data['isSmoker'],
		    'cigarettesPerDay' => $data['cigarettesPerDay'], 
			'alcoholUnitsPerWeek' => $data['alcoholUnitsPerWeek'], 
			'exerciseTimesPerWeek' => $data['exerciseTimesPerWeek'],
		    'exerciseType' => $data['exerciseType'], 
			//'patientType' => $data['patientType']
            		
		]);
		
		return response()->json([
		    'patientInfo' => $p
		]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		return response()->json([
		    
             'patientInfo' => PatientInfo::firstOrNew(['user_id' => $id]),
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		return response()->json([
		    'data' => PatientInfo::find($id),
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //id here is the user id and not ptient info id
		$data = $request->all();
		
		$p = PatientInfo::updateOrCreate(['user_id' => $id],[
		    //'user_id' => $data['user_id'], 
			'tribe'=> $data['tribe'], 
			'height' => $data['height'], 
			'weight'  => $data['weight'], 
			'notes'  => $data['notes'], 
			'isSmoker' => $data['isSmoker'],
		    'cigarettesPerDay' => $data['cigarettesPerDay'], 
			'alcoholUnitsPerWeek' => $data['alcoholUnitsPerWeek'], 
			'exerciseTimesPerWeek' => $data['exerciseTimesPerWeek'],
		    'exerciseType' => $data['exerciseType'], 
			'patientType' => 'check group on user'
            		
		]);
		
		return response()->json([
		    'patientInfo' => $p,
		]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$info = PatientInfo::find($id);
		$info->delete();
		
		return response()->json([
		    'data' => 'success',
		]);
    }
}
