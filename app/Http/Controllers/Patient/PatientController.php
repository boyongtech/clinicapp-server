<?php

namespace App\Http\Controllers\Patient;

use App\User;
use Excel;
use File;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		return response()->json([
		    'patients' => User::where('group', 'Out patient')->orWhere('group', 'In patient')->get(),
			'inPatient' => User::where('group', 'In patient')->get(),
			'outPatient' => User::where('group', 'Out patient')->get()
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$data = $request->all();
		
		$patient = User::create([
		    'title' => $data['title'],
		    'firstname' => $data['firstname'], 
			'middlename' => isset($data['middlename'])? $data['middlename'] : '', 
			'lastname' => $data['lastname'], 
			'email' => $data['email'], 
			'password' => bcrypt($data['password']),
		    'town' => isset($data['town'])? $data['town'] : '' , 
			'address' => $data['address'], 
			'homePhone' => $data['homePhone'], 
			'mobile' => $data['mobile'], 
			'maritalStatus' => $data['maritalStatus'],
		    'gender' => $data['gender'], 
			//'picture' => $data['picture'], 
			'group' => $data['group'],
			'dob' => isset($data['dob'])? $data['dob'] : ''
		]);
		
		
		return response()->json([
		    'patient_id' => $patient->id
		]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		return response()->json([
		    'patient' => User::find($id)
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$data = $request->all();
		$patient = User::where('id', $id)->update([
		    'title' => $data['title'],
		    'firstname' => $data['firstname'], 
			'middlename' => $data['middlename'], 
			'lastname' => $data['lastname'], 
			'email' => $data['email'], 
			//'password' => bcrypt($data['password']),
		    'town' => $data['town'], 
			'address' => $data['address'], 
			'homePhone' => $data['homePhone'], 
			'mobile' => $data['mobile'], 
			'maritalStatus' => $data['maritalStatus'],
		    'gender' => $data['gender'], 
		//	'picture' => $data['picture'], 
			'group' => $data['group'],
			'dob' => $data['dob']
		]);
		
		return response()->json([
		    'patient'=> User::find($id)
		]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function photoUpload(Request $request, $id)
	{
		$user = User::find($id);
	   if($request->hasFile('file')){
		   
		   
	       $path = $request->file->storeAs('images/avatar', 'photo_'. $user->firstname .'_'. $id . '.'. $request->file->extension() );
		   $user->picture = $path;
		   $user->save();
		   
	       return [$path, Storage::url($path)];
	   }
		
	   return 'no';
	}
	
	
	//get user photo
	public function userPhoto($id)
	{
		$user  = User::find($id);
		$path = $user->picture;
		
	    if (!empty($path))
	    {
		
		    $file = File::get(storage_path() . '/app/' .$path);
		    $type = File::mimeType(storage_path() . '/app/' .$path);
		
		    $response = Response::make($file, 200);
		    $response->header("Content-Type", $type);
		    
		    return $response;  
	    }
	    
	    return $path;
		
		    
		
	}
	
	public function patientListImport(Request $request)
	{
		if($request->hasFile('file')){
		
			$path = $request->file('file')->getRealPath();
		
		
			$data = Excel::load($path, function($reader) {})->get();
		
		
			if(!empty($data) && $data->count()){
		
		
				foreach ($data->toArray() as $key => $value) {
		
					if(!empty($value)){
		              var_dump($value);
						foreach ($value as $v) {
		
							$insert[] = [
									'title' => 'Mr',
									'firstname' => $v['first_name'], 
									'lastname' => $v['last_name'],
									'gender' => $v['gender'],
									'dob' => $v['dob'],
									'group' => $v['group'],
									'address' => $v['address'],
									'password'=> 'none',
									'email' => $v['email']
									
							];
		
						}
		
					}
		
				}
		
		
		
		
				if(!empty($insert)){
		
					User::insert($insert);
		
					return response()->json([
							'patients' => User::where('group', 'Out patient')->orWhere('group', 'In patient')->get(),
							'inPatient' => User::where('group', 'In patient')->get(),
							'outPatient' => User::where('group', 'Out patient')->get()
					]);
		
				}
		
		
			}
		
		
		}
		
		
		return 'error: Please Check your file, Something is wrong there.';

	}
	
	
	
	public function invoice($id)
	{
	
	}
}
