<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

// APIs to be relocated soon


//patient module
Route::resource('patient-info', 'Patient\PatientInfoController');
Route::resource('patient', 'Patient\PatientController');
Route::post('patient-photo/upload/{id}', 'Patient\PatientController@photoUpload');
Route::post('patient-list/import', 'Patient\PatientController@patientListImport');

Route::get('user-photo/{id}', 'Patient\PatientController@userPhoto');


Route::get('patient-invoice/{id}', 'Patient\PatientController@invoice');

//appointment module
Route::get('appointment/doctor/{id}', 'General\AppointmentController@doctorAppointments');
Route::resource('appointment', 'General\AppointmentController');
Route::get('patient-appointment/{id}', 'General\AppointmentController@patientAppointment');

//staff module
Route::resource('staff', 'Staff\StaffController');
Route::resource('doctor', 'Doctor\DoctorInfoController');

//appointment module
Route::get('serve-patient', 'General\AppointmentController@servePatient');
Route::get('serve-doctor', 'General\AppointmentController@serveDoctorOrNurse');


//prescription module
Route::resource('prescriptions', 'Prescription\PrescriptionsController');
Route::get('patient-prescription/{id}', 'Prescription\PrescriptionsController@patientPrescription');
Route::resource('treatments', 'Prescription\TreatmentsController');
Route::get('serve-treatment', 'Prescription\TreatmentsController@serveTreatment');

//Ambulance

//Route::group(['prefix' => 'v1'], function() {
Route::resource('ambulance', 'Ambulance\AmbulanceController');
Route::resource('ambulance-price','Ambulance\AmbulancePriceController');
Route::resource('ambulance-request','Ambulance\AmbulanceRequestController');
Route::resource('ambulance-service','Ambulance\AmbulanceServiceController');

Route::get('serve-driver', 'Ambulance\AmbulanceController@serveDriver');
Route::get('serve-price/{am_id}/{re_id}', 'Ambulance\AmbulanceServiceController@servePriceInfo');



//});


